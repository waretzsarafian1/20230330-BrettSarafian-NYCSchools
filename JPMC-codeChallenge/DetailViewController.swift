//
//  DetailViewController.swift
//  JPMC-codeChallenge
//
//  Created by Brett Sarafian on 3/29/23.
//

import UIKit
import MapKit

final class DetailViewController: UIViewController {
    let satScores: SATScores?
    let school: School

    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let schoolMapView: MKMapView = {
        let view = MKMapView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: SAT Scores Labels
    let satLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Sat scores".uppercased()
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 17, weight: .bold)
        return view
    }()

    let numberLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Students tested".uppercased()
        view.font = .systemFont(ofSize: 10, weight: .bold)
        view.numberOfLines = 2
        view.textAlignment = .center
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let numberValueLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 16, weight: .regular)
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        view.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 4).isActive = true
        return view
    }()

    let readingLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.text = "Reading \nAverage".uppercased()
        view.font = .systemFont(ofSize: 10, weight: .bold)
        view.textAlignment = .center
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let readingValueLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 16, weight: .regular)
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let mathLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.text = "Math \nAverage".uppercased()
        view.font = .systemFont(ofSize: 10, weight: .bold)
        view.textAlignment = .center
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let mathValueLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 16, weight: .regular)
        view.textAlignment = .center
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let writingLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.text = "Writing \nAverage".uppercased()
        view.font = .systemFont(ofSize: 10, weight: .bold)
        view.textAlignment = .center
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let writingValueLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 16, weight: .regular)
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()

    let descriptionTitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 16, weight: .bold)
        view.textAlignment = .center
        view.text = "About this school".uppercased()
        return view
    }()

    let descriptionLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .clear
        view.numberOfLines = 0
        view.font = .systemFont(ofSize: 16, weight: .light)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let contactButton: UIButton = {
        let view = UIButton()
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Contact School", for: .normal)
        view.backgroundColor = .black
        return view
    }()

    init(school: School, satScores: SATScores?) {
        self.school = school
        self.satScores = satScores
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupScroll()
        setupViews()
        setupMap()
    }

    // MARK: Setups
    private func setupScroll() {
        self.view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            scrollView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            scrollView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            scrollView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height)
        ])
    }

    private func setupUI() {
        self.title = school.schoolName
        view.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .black
        descriptionLabel.text = school.overviewParagraph
        numberValueLabel.text = satScores?.numberTaken ?? "0"
        readingValueLabel.text = satScores?.readingScore ?? "0"
        mathValueLabel.text = satScores?.mathScore ?? "0"
        writingValueLabel.text = satScores?.writingScore ?? "0"
        contactButton.addTarget(self, action: #selector(contactButtonAction), for: .touchUpInside)
    }

    private func setupMap() {
        let latitude = try? Double(school.latitude ?? "0", format: .number)
        let longitude = try? Double(school.longitude ?? "0", format: .number)
        let center = CLLocationCoordinate2D(latitude: latitude ?? 0, longitude: longitude ?? 0)
        let region = MKCoordinateRegion(center: center, latitudinalMeters: CLLocationDistance(exactly: 500)!, longitudinalMeters: CLLocationDistance(exactly: 500)!)
        schoolMapView.setRegion(region, animated: true)

        let annotation = MKPointAnnotation()
        annotation.coordinate = center
        schoolMapView.addAnnotation(annotation)
    }

    private func setupViews() {
        let margin: CGFloat = 8

        let safe = scrollView
        scrollView.addSubview(schoolMapView)
        NSLayoutConstraint.activate([
            schoolMapView.topAnchor.constraint(equalTo: safe.topAnchor),
            schoolMapView.centerXAnchor.constraint(equalTo: safe.centerXAnchor),
            schoolMapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            schoolMapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            schoolMapView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            schoolMapView.widthAnchor.constraint(equalTo: schoolMapView.heightAnchor)
        ])

        scrollView.addSubview(satLabel)
        NSLayoutConstraint.activate([
            satLabel.topAnchor.constraint(equalTo: schoolMapView.bottomAnchor, constant: 10),
            satLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            satLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            satLabel.heightAnchor.constraint(equalToConstant: 44)
        ])

        // MARK: SAT Labels
        scrollView.addSubview(numberValueLabel)
        scrollView.addSubview(readingValueLabel)
        scrollView.addSubview(mathValueLabel)
        scrollView.addSubview(writingValueLabel)

        NSLayoutConstraint.activate([
            numberValueLabel.topAnchor.constraint(equalTo: satLabel.bottomAnchor, constant: 10),
            numberValueLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),

            readingValueLabel.topAnchor.constraint(equalTo: numberValueLabel.topAnchor),
            readingValueLabel.leadingAnchor.constraint(equalTo: numberValueLabel.trailingAnchor),
            readingValueLabel.widthAnchor.constraint(equalTo: numberValueLabel.widthAnchor),

            mathValueLabel.topAnchor.constraint(equalTo: numberValueLabel.topAnchor),
            mathValueLabel.leadingAnchor.constraint(equalTo: readingValueLabel.trailingAnchor),
            mathValueLabel.widthAnchor.constraint(equalTo: numberValueLabel.widthAnchor),

            writingValueLabel.topAnchor.constraint(equalTo: numberValueLabel.topAnchor),
            writingValueLabel.leadingAnchor.constraint(equalTo: writingValueLabel.trailingAnchor),
            writingValueLabel.widthAnchor.constraint(equalTo: numberValueLabel.widthAnchor),
            writingValueLabel.trailingAnchor.constraint(equalTo: safe.trailingAnchor, constant: -margin)
        ])

        scrollView.addSubview(numberLabel)
        scrollView.addSubview(readingLabel)
        scrollView.addSubview(mathLabel)
        scrollView.addSubview(writingLabel)

        NSLayoutConstraint.activate([
            numberLabel.topAnchor.constraint(equalTo: numberValueLabel.bottomAnchor),
            numberLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),
            numberLabel.widthAnchor.constraint(equalTo: numberValueLabel.widthAnchor),

            readingLabel.topAnchor.constraint(equalTo: numberLabel.topAnchor),
            readingLabel.leadingAnchor.constraint(equalTo: numberLabel.trailingAnchor),
            readingLabel.widthAnchor.constraint(equalTo: numberLabel.widthAnchor),

            mathLabel.topAnchor.constraint(equalTo: numberLabel.topAnchor),
            mathLabel.leadingAnchor.constraint(equalTo: readingLabel.trailingAnchor),
            mathLabel.widthAnchor.constraint(equalTo: numberLabel.widthAnchor),

            writingLabel.topAnchor.constraint(equalTo: numberLabel.topAnchor),
            writingLabel.leadingAnchor.constraint(equalTo: mathLabel.trailingAnchor),
            writingLabel.widthAnchor.constraint(equalTo: numberLabel.widthAnchor),
            writingLabel.trailingAnchor.constraint(equalTo: safe.trailingAnchor, constant: -margin)
        ])

        // MARK: School Description
        scrollView.addSubview(descriptionTitleLabel)
        NSLayoutConstraint.activate([
            descriptionTitleLabel.topAnchor.constraint(equalTo: numberLabel.bottomAnchor, constant: 20),
            descriptionTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),
            descriptionTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -margin),
        ])

        scrollView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: descriptionTitleLabel.bottomAnchor, constant: 20),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: margin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -margin),
            descriptionLabel.bottomAnchor.constraint(equalTo: safe.bottomAnchor, constant: -60)
        ])

        // MARK: Contact
        self.view.addSubview(contactButton)
        NSLayoutConstraint.activate([
            contactButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            contactButton.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: margin * 2),
            contactButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -margin * 2),
            contactButton.heightAnchor.constraint(equalToConstant: 44),
        ])
    }

    // MARK: Actions
    @objc private func contactButtonAction() {
        let alertVC = UIAlertController(title: "Contact".uppercased(), message: "", preferredStyle: .actionSheet)
        // Phone Action
        if let phoneNumber = school.phoneNumber {
            let phoneAction = UIAlertAction(title: "Call \(phoneNumber)", style: .default) { _ in
                self.callSchoolPhone(phoneNumber)
            }
            alertVC.addAction(phoneAction)
        }

        // Website Action
        if let website = school.website {
            let webAction = UIAlertAction(title: "Visit \(school.schoolName)", style: .default) { _ in
                self.visitWebsite(website)
            }
            alertVC.addAction(webAction)
        }

        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))

        self.present(alertVC, animated: true)
    }

    @objc private func callSchoolPhone(_ phoneString: String) {
        guard let phoneCallURL = URL(string: "tel://\(phoneString)"),
                UIApplication.shared.canOpenURL(phoneCallURL) else { return }

        UIApplication.shared.open(phoneCallURL)
    }

    @objc private func visitWebsite(_ urlString: String) {
        if let websiteURL = URL(string: "https://\(urlString)") {
            UIApplication.shared.open(websiteURL)
        }
    }
}
