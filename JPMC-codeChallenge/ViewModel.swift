//
//  ListViewModel.swift
//  JPMC-codeChallenge
//
//  Created by Brett Sarafian on 3/29/23.
//

import Foundation

final class ViewModel {
    var reuseIdentifier = "school"
    var title = "NYC High Schools"
    var schoolsData: Data?
    var schools: [School] = []
    var sats = [String : SATScores]()

    var schoolsURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    var satURLString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

    init() {
        getData { _ in }
        getSATData()
    }

    func getData(_ completionHandler: @escaping (Result<Data, Error>) -> Void) {
        if let schoolsData = schoolsData {
            completionHandler(.success(schoolsData))
            return
        }

        guard let url = URL(string: schoolsURLString) else { return }

        let request = URLRequest(url: url)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(.failure(error))
                }

                if let data = data {
                    self.schoolsData = data
                    completionHandler(.success(data))
                    return
                }
            }
        }
        task.resume()
    }

    func parseData(data: Data) {
        guard schools.isEmpty else { return }
        do {
            let decoder = JSONDecoder()
            let schoolData = try decoder.decode([School].self, from: data)
                self.schools = schoolData
        } catch {
            print("Error occured: \(error)")
        }
    }

    private func getSATData() {
        guard let url = URL(string: satURLString) else { return }

        let request = URLRequest(url: url)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    print("Error occured: \(error)")
                }

                if let data = data {
                    self.parseSATData(data: data)
                    return
                }
            }
        }
        task.resume()
    }

    private func parseSATData(data: Data) {
        do {
            let decoder = JSONDecoder()
            let satData = try decoder.decode([SATScores].self, from: data)
            sats = satData.reduce(into: [String: SATScores](), { result, scores in
                result[scores.dbn] = scores
            })
        } catch {
            print("Error occured: \(error)")
        }
    }
}
