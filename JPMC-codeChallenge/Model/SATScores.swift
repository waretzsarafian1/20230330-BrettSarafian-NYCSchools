//
//  SATScores.swift
//  JPMC-codeChallenge
//
//  Created by Brett Sarafian on 3/29/23.
//

import UIKit

struct SATScores: Hashable, Codable {
    var dbn: String
    var schoolName: String
    var numberTaken: String?
    var readingScore: String?
    var mathScore: String?
    var writingScore: String?

    static func == (lhs: SATScores, rhs: SATScores) -> Bool {
        return lhs.dbn == rhs.dbn
    }

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numberTaken = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
