//
//  School.swift
//  JPMC-codeChallenge
//
//  Created by Brett Sarafian on 3/29/23.
//

import Foundation

struct School: Hashable, Codable {
    var dbn: String
    var schoolName: String
    var boro: String?
    var overviewParagraph: String?

    var neighborhood: String?

    var latitude: String?
    var longitude: String?
    var phoneNumber: String?
    var schoolEmail: String?
    var website: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"

        case neighborhood
        case latitude
        case longitude

        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website

    }

    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.dbn == rhs.dbn
    }
}


