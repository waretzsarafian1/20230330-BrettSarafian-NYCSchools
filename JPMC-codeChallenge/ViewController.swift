//
//  ListViewController.swift
//  JPMC-codeChallenge
//
//  Created by Brett Sarafian on 3/29/23.
//

import UIKit

final class ViewController: UIViewController {
    typealias DataSource = UITableViewDiffableDataSource<Int, School>

    lazy var dataSource: DataSource = configDataSource()

    var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = .clear
        view.separatorColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    var viewModel: ViewModel

    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.getData { result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self.viewModel.parseData(data: data)
                    self.updateSnapshot()
                }

            case .failure(let error):
                print(error)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.title
        setupTable()
        applyInitialSnapshot()
        view.backgroundColor = .white
    }

    private func setupTable() {
        let safeArea = view.safeAreaLayoutGuide

        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
        ])
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: viewModel.reuseIdentifier)
    }

    private func configDataSource() -> DataSource {
        return DataSource(tableView: tableView) { tableView, indexPath, school in
            let cell = tableView.dequeueReusableCell(withIdentifier: self.viewModel.reuseIdentifier, for: indexPath)
            var content = cell.defaultContentConfiguration()
            content.text = school.schoolName
            content.textProperties.font = .systemFont(ofSize: 17, weight: .bold)
            content.secondaryText = school.dbn
            content.secondaryTextProperties.font = .systemFont(ofSize: 15, weight: .bold)
            content.secondaryTextProperties.color = .darkGray
            cell.contentConfiguration = content
            cell.contentView.backgroundColor = .white
            return cell
        }
    }

    private func applyInitialSnapshot() {
        var initialSnapshot = NSDiffableDataSourceSnapshot<Int, School>()
        initialSnapshot.appendSections([0])
        initialSnapshot.appendItems([])
        dataSource.apply(initialSnapshot)
    }

    func updateSnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, School>()
        snapshot.appendSections([0])
        snapshot.appendItems(viewModel.schools)
        dataSource.applySnapshotUsingReloadData(snapshot)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.schools[indexPath.row]
        let satScores = viewModel.sats[school.dbn]
        let viewController = DetailViewController(school: school, satScores: satScores)
        navigationController?.pushViewController(viewController, animated: true)
    }
}


